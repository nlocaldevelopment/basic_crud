lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'basic_crud/version'
require 'date'

Gem::Specification.new do |spec|
  spec.name          = "basic_crud"
  spec.version       = BasicCrud::VERSION
  spec.date          = Date.today.to_s
  spec.authors       = ["Raul_Cabrera"]
  spec.email         = ["raul.cabrera@publicar.com"]

  spec.summary       = %q{Make more easy basic crud creation for apis}
  spec.description   = %q{This add basic crud to controllers}
  spec.homepage      = 'https://bitbucket.org/nlocaldevelopment/basic_crud.git'
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # development dependencies
  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.5"
  spec.add_development_dependency "rspec", "~> 3.7"
  spec.add_development_dependency "shoulda-matchers", "~> 3.1"
  # spec.add_development_dependency "factory_bot", '~> 4.10'
  spec.add_development_dependency "pry", '~> 0.11'
  spec.add_development_dependency "rubocop", '~> 0.58'
  # run time dependencies
  spec.add_runtime_dependency "rails"
  # dependencies
  spec.add_dependency "lp_csv_exportable", '~> 0.2'
end
