require 'set'

# shared examples
Dir["./spec/support/**/*.rb"].sort.each {|f| require f}

# factories
Dir["./spec/factories/**/*.rb"].sort.each {|f| require f}