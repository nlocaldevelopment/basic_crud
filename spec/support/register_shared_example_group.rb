## important because we set routes dinamically with this register info !!!
RSpec.shared_examples_for 'a register' do |base, descendant|
  # define a kontroller base and descendant

  it 'self is added to a basic crud registry' do
    expect(BasicCrud::REGISTRY).to include(base.class)
  end

  it 'descendants are not registered' do
    expect(descendant.class.superclass).to eq(base.class)
    expect(base.class.descendants).to include(descendant.class)
    expect(BasicCrud::REGISTRY).not_to include(descendant.class)
  end

  context 'but a base controller knows..' do
    it 'that is a base controller with descendants model controllers' do
      expect(base.class.direct_descendants).to include(descendant.class)
    end
    it 'that it hasnt a valid orm model' do
      expect(base.class.send(:has_valid_model?)).to be false
    end
  end

  context 'but a model controller knows..' do
    it 'that is a model controller' do
      expect(descendant.class.direct_descendants).to be_empty
    end

    context 'that has a valid orm model defined..' do
      it 'with model namespace' do
        module Api; class Model < ActiveRecord::Base; end; end
        descendant.class.define_model_namespace('Api')
        expect(descendant.class.send(:has_valid_model?)).to be true
      end
      it 'without model namespace' do
        descendant.class.define_model_namespace('')
        expect(descendant.class.send(:has_valid_model?)).to be true
      end
    end
  end
end