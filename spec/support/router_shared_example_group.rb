RSpec.shared_examples_for 'a router mapper' do |router, controller|
  # define a router and controller

  mapper              = ActionDispatch::Routing::Mapper.new(router)

  crud                = %i{ index show create update destroy }
  massive_crud        = %i{ massive_update massive_destroy }
  soft_delete         = %i{ soft_delete recover }
  massive_soft_delete = %i{ massive_soft_delete massive_recover }

  models              = mapper.return_registered_controllers
  crud_routes         = controller.class.registered_actions.map do |action|
                          models.map do |model|
                            model = model.name.underscore
                            action == :index ? model.pluralize : "#{model}_#{action}"
                          end
                        end.flatten

  it "#{mapper.class} respond_to #mount_basic_crud" do
    expect(mapper).to respond_to(:mount_basic_crud)
  end

  describe 'Controller', type: :controller do
    it 'registers actions' do
      controller.class.register_actions *crud
      expect(controller.class.registered_actions).to include(*crud)
    end
  end

  describe 'Router..' do
    it 'draws new routes' do
      controller.class.register_actions *crud
      expect{
        router.draw do
          mount_basic_crud
        end
      }.to change{ router.routes.count }
    end

    context 'with an api version number..' do
      it 'by default' do
        expect(controller.class.api[:version]).to eq(1)
      end
      it 'set by hand' do
        version = 2
        controller.class.define_api version: version
        expect(controller.class.api[:version]).to eq(version)
      end
      # reset version
      controller.class.define_api version: 1
    end

    context 'without soft delete methods defined' do
      it 'creates a basic crud routes' do
        router.draw do
          mount_basic_crud
        end
        controller.class.register_actions *crud
        routes = router.routes.map{|route| route.path.spec.left.memo.name}
        expect(routes).to include(*crud_routes)
      end

      context 'when massive is..' do
        it 'disabled, raise error' do
          controller.class.register_actions *massive_crud
          expect{
            router.draw do
              mount_basic_crud
            end
          }.to raise_error(RuntimeError)
        end

        it 'enabled, creates massive routes' do
          controller.class.register_actions *massive_crud
          controller.class.enable_massive_actions
          router.draw do
            mount_basic_crud
          end
          routes = router.routes.map{|route| route.path.spec.left.memo.name}
          expect(routes).to include(*crud_routes)
        end
      end
    end

    context 'with soft delete methods defined' do
      context 'when model..' do
        it 'doesnt respond to soft delete method, raise an error' do
          controller.class.register_actions *soft_delete
          controller.class.define_delete_methods(
            soft_delete: { method: :discard },
            recover:     { method: :undiscard }
          )
          expect{
            router.draw do
              mount_basic_crud
            end
          }.to raise_error(RuntimeError)
        end

        context 'responds to soft delete method,' do
          it 'creates a soft delete routes' do
            controller.class.register_actions *soft_delete
            controller.class.klass_model.class_eval do
              def discard;   end
              def undiscard; end
            end
            controller.class.define_delete_methods(
              soft_delete: { method: :discard },
              recover:     { method: :undiscard }
            )
            router.draw do
              mount_basic_crud
            end
            routes = router.routes.map{|route| route.path.spec.left.memo.name}
            expect(routes).to include(*crud_routes)
          end

          context 'when massive is..' do
            it 'disabled, raise error' do
              controller.class.register_actions *massive_soft_delete
              controller.class.massive = false
              controller.class.klass_model.class_eval do
                def discard;   end
                def undiscard; end
              end
              controller.class.define_delete_methods(
                soft_delete: { method: :discard },
                recover:     { method: :undiscard }
              )
              expect{
                router.draw do
                  mount_basic_crud
                end
              }.to raise_error(RuntimeError)
            end

            it 'enabled, creates massive routes' do
              controller.class.register_actions *massive_soft_delete
              controller.class.enable_massive_actions
              controller.class.klass_model.class_eval do
                def discard;   end
                def undiscard; end
              end
              controller.class.define_delete_methods(
                soft_delete: { method: :discard },
                recover:     { method: :undiscard }
              )
              router.draw do
                mount_basic_crud
              end
              routes = router.routes.map{|route| route.path.spec.left.memo.name}
              expect(routes).to include(*crud_routes)
            end
          end
        end
      end
    end
  end
end