RSpec.shared_examples_for 'a error handler' do |controller|
  # define a kontroller instance like subject
  subject { controller }

  it 'has setted all defined rescue handlers' do
    error_types = subject.rescue_handlers.map do |handler|
      handler[0].constantize # error type
    end
    expect(BasicCrud::Errors::TYPES).to eq(error_types)
  end

  context 'respond to..' do
    BasicCrud::Errors::TYPES.each do |error|
      handler = error.to_s.demodulize.underscore.to_sym

      it "handler method \##{handler}" do
        expect(subject).to respond_to(handler)
      end
      it { should rescue_from(error).with(handler) }
    end
  end
end