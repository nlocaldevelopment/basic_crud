require "pry"
require "bundler/setup"
require "basic_crud"
require "shoulda/matchers"
# require "factory_bot"

require "./spec/test_helpers"

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    # Choose a test framework:
    with.test_framework :rspec

    # Or, choose the following (which implies all of the above):
    with.library :rails
  end
end

RSpec.configure do |config|
  # factory bot
  # config.include FactoryBot::Syntax::Methods
  # config.before(:suite) do
  #   FactoryBot.find_definitions
  # end

  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end