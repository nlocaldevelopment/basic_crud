RSpec.describe BasicCrud do
  it "has a version number" do
    expect(BasicCrud::VERSION).not_to be nil
  end

  context 'when is used with a..' do
    describe 'Class', type: :model do
      it 'raise an error' do
        expect{ Class.include(BasicCrud) }.to raise_error(RuntimeError)
      end
    end
  end

  context 'when is used with a..' do
    describe 'Controller', type: :controller do
      router = ActionDispatch::Routing::RouteSet.new

      it_behaves_like 'a register',      BaseController.include(BasicCrud).new, ModelsController.new
      it_behaves_like 'a router mapper', router, ModelsController.include(BasicCrud).new
    end
  end
end