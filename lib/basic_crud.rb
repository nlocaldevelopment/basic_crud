# dependencies
require 'active_record'
require 'action_controller'
require 'active_support'
require 'active_support/all'
require 'lp_csv_exportable'

require 'basic_crud/version'
require 'basic_crud/controllers/concerns/errors_handler'
require 'basic_crud/controllers/concerns/crud'
require 'basic_crud/router/api_constraint'
require 'basic_crud/router/mapper'
require 'basic_crud/exporter/csv'

module BasicCrud
  # Your code goes here...
end
