module BasicCrud
  module Mapper
    extend ::ActiveSupport::Concern

    def mount_basic_crud
      return_registered_controllers.each do |controller|
        _model      = controller.klass_model.name.demodulize.underscore
        _controller = _model.pluralize.to_sym

        scope module: 'api' do
          scope module: "v#{controller.api[:version]}", constraints: ApiConstraint.new(controller.api) do
            controller.registered_actions.each do |action|
              case action
                # basic crud routes
                when :index
                  post    "#{_controller}/search",                    action: :index,   controller: _controller if controller.search_via_json?
                  get     "#{_controller}",                           action: :index,   controller: _controller
                when :show
                  get    "#{_model}/show",                            action: :show,    controller: _controller
                  get    "#{_controller}/:#{controller.primary_key}", action: :show,    controller: _controller
                when :create
                  post   "#{_model}/create",                          action: :create,  controller: _controller
                  post   "#{_controller}",                            action: :create,  controller: _controller
                when :destroy
                  delete "#{_model}/destroy",                         action: :destroy, controller: _controller
                  delete "#{_controller}/:#{controller.primary_key}", action: :destroy, controller: _controller
                when :update
                  match  "/#{_model}/update",                          :to => "#{_controller}#update", via: [:put, :patch]
                  match  "/#{_controller}/:#{controller.primary_key}", :to => "#{_controller}#update", via: [:put, :patch]

                # massives routes
                when :massive_destroy
                  raise "Massive is not enabled for #{controller}" unless controller.massive?
                  delete "/#{_model}/massive_destroy", action: :massive_destroy, controller: _controller
                when :massive_update
                  raise "Massive is not enabled for #{controller}" unless controller.massive?
                  match "/#{_model}/massive_update", :to => "#{_controller}#massive_update", via: [:put, :patch]

                # soft delete routes
                when :soft_delete
                  raise "#{controller} doesnt respond to soft delete method" unless controller.soft_delete_method_enabled?
                  delete "#{_model}/soft_delete", action: :soft_delete, controller: _controller
                when :recover
                  raise "#{controller} doesnt respond to soft delete method" unless controller.soft_delete_method_enabled?
                  match "/#{_model}/recover", :to => "#{_controller}#recover",  via: [:put, :patch]

                # massive soft delete routes
                when :massive_soft_delete
                  raise "Massive is not enabled for #{controller}" unless controller.massive?
                  raise "#{controller} doesnt respond to soft delete method" unless controller.soft_delete_method_enabled?
                  delete "#{_model}/massive_soft_delete", action: :massive_soft_delete, controller: _controller
                when :massive_recover
                  raise "Massive is not enabled for #{controller}" unless controller.massive?
                  raise "#{controller} doesnt respond to soft delete method" unless controller.soft_delete_method_enabled?
                  match "/#{_model}/massive_recover", :to => "#{_controller}#massive_recover", via: [:put, :patch]
              end
            end

            if controller.method_defined?(:rebuild)
              get "#{_model}/rebuild", action: :rebuild, controller: _controller
            end
          end
        end
      end
    end

    def return_registered_controllers
      BasicCrud::REGISTRY.map do |controller|
        if controller.has_valid_model?
          controller
        else
          models = controller.direct_descendants
          raise "This Controller #{controller.name} has not a valid model" unless models.any?
          models
        end
      end.flatten
    end
  end
end