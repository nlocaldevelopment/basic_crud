class ApiConstraint
  attr_reader :version, :vendor, :default

  def initialize(options)
    @version = options.fetch(:version)
    @default = options.fetch(:default)
  end

  def matches?(request)
    (request.headers.try(:fetch, :accept).try(:include?,media_type) ||
    (@default  && request.headers.try(:fetch, :accept).try(:split,",").try(:map, &:lstrip ).try(:any?){|d| d.match(/^application\/v\d$/).blank?})
    )
  rescue => e
    false
  end

  private

  def media_type
    "application/v#{@version}"
  end
end