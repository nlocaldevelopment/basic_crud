# monkey patch for gem lp_csv_exportable to set col_sep
module BasicCrud::Exporter
  module CSV
    include ::LpCSVExportable::CanExportAsCSV

    def to_csv
      ::CSV.generate(col_sep: ';') do |csv|
        csv << headers
        data_matrix.each do |row|
          csv << row
        end
      end
    end
  end
end