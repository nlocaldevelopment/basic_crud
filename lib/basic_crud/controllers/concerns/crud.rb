module BasicCrud
  extend ::ActiveSupport::Concern

  # used to set dinamically basic crud routes
  REGISTRY = []

  included do
    # initial control
    raise 'This model is not a Controller' unless a_controller?

    # autoexec
    register_controller

    # autoload modules
    include BasicCrud::Errors
    # monkey patch
    ::ActionDispatch::Routing::Mapper.send(:include, BasicCrud::Mapper)
    ::ActionController::API.send(:include, ActionController::MimeResponds)

    # class attributes
    class_attribute :api
    class_attribute :primary_key
    class_attribute :registered_actions
    class_attribute :enabled_attributes
    class_attribute :massive
    class_attribute :model_namespace
    class_attribute :delete_methods
    class_attribute :available_serializer
    class_attribute :available_exporter
    class_attribute :paginate_state
    class_attribute :scopes_filter
    class_attribute :return_disabled_records
    class_attribute :search_via_json

    # default values
    self.api                      = { version: 1, default: true }
    self.primary_key              = :id
    self.registered_actions       = []
    self.enabled_attributes       = nil # idem to get all
    self.massive                  = false
    self.model_namespace          = ''
    self.delete_methods           = {
                                      destroy:     { method: :destroy, scope: :all },
                                      soft_delete: { method: nil, scope: nil },
                                      recover:     { method: nil, scope: nil }
                                    }
    self.return_disabled_records  = false
    self.search_via_json          = false
    self.scopes_filter            = {
                                      records: [],
                                      record:  [ { method: :find_by, attributes: self.primary_key } ]
                                    }
    self.available_serializer     = nil
    self.available_exporter       = nil
    self.paginate_state           = false

    # controller configuration methods
    def self.define_api(options)
      options.keys.each do |key|
        self.api[key] = options[key]
      end
    end

    def self.define_primary_key(attribute)
      self.primary_key = attribute
    end

    def self.register_actions(*actions)
      self.registered_actions = actions
    end

    def self.enable_attributes(*attributes)
      self.enabled_attributes = attributes
    end

    def self.enable_massive_actions
      self.massive = true
    end

    def self.enable_search_via_json
      self.search_via_json = true
    end

    def self.define_model_namespace(namespace=nil)
      self.model_namespace =  if namespace
                                namespace.is_a?(Class) ? '::' + namespace.to_s : namespace
                              else
                                self.name.deconstantize
                              end
    end

    def self.define_delete_methods(**actions)
      %i{ destroy soft_delete recover }.each do |action|
        self.delete_methods[action] = hash_values_to_sym(actions[action]) unless actions[action].nil?
      end
      create_delete_methods
    end

    def self.define_scope_filters(**scopes)
      %i{ records record }.each do |key|
        self.scopes_filter[key] = hash_values_to_sym(scopes[key]) unless scopes[key].nil?
      end
    end

    def self.model_serializer(serializer)
      serializer = serializer.is_a?(Class) ? serializer.name.demodulize : serializer.demodulize
      self.available_serializer = ("#{model_namespace}::#{serializer}").constantize
    end

    def self.model_exporter(exporter)
      exporter = exporter.is_a?(Class) ? exporter.name.demodulize : exporter.demodulize
      self.available_exporter = ("#{model_namespace}::#{exporter}").constantize

      # autoload libs
      # self.available_exporter.send(:include, LpCSVExportable::CanExportAsCSV)
      self.available_exporter.send(:include, BasicCrud::Exporter::CSV) # monkey patch .to_csv method
    end

    def self.enable_paginate_records
      self.paginate_state = true
    end

    def self.enable_return_disabled_records
      self.return_disabled_records = true
    end

    # callbacks
    before_action :check_registered_actions
    before_action :get_records,              only: [:index, :massive_update, :massive_soft_delete, :massive_destroy, :massive_recover]
    before_action :new_record,               only: :create
    before_action :get_record,               only: [:show, :update, :destroy, :soft_delete, :recover]
  end

  module ClassMethods

    def klass_model
      namespace = model_namespace.present? ? "#{model_namespace}::" : model_namespace
      model = (namespace + name.demodulize.gsub('Controller','').singularize)
      model.constantize rescue nil
    end

    def has_valid_model?
      defined?(klass_model) and klass_model.respond_to?(:create) rescue false
    end

    def soft_delete_method_enabled?
      %i{ soft_delete recover }.all? do |action|
        delete_methods[action][:method] ? klass_model.method_defined?(delete_methods[action][:method].to_sym) : false
      end
    end

    private

    def a_controller?
      ancestors.first.respond_to?(:controller_name)
    end

    def register_controller
      REGISTRY << ancestors.first
    end

    def hash_values_to_sym(data)
      if data.is_a?(Array)
        data.map{|d| hash_values_to_sym(d)}
      else
        data.each do |k,values|
          data[k] = if values.is_a?(Array)
            values.map do |value|
              value.is_a?(Hash) ? value : value.try(:to_sym)
            end
          else
            value = values
            value.try(:to_sym)
          end
        end
      end
    end

    def create_delete_methods
      class_eval do
        delete_methods.each do |key,value|
          if value[:method]
            # singular delete
            define_method(key.to_sym) do
              raise StandardError.new("#{@record.class} doesnt respond to \##{value[:method]}") unless @record.respond_to?(value[:method].to_sym)
              begin
                @record.send(value[:method].to_sym)
                serializer_method(@record)
              rescue => e
                render json: { :error => e.message }, status: :unprocessable_entity
              end
            end
            if massive
              # massive deletes
              define_method("massive_#{key}".to_sym) do
                raise StandardError.new("#{self.class.klass_model} doesnt respond to \##{value[:method]}") unless @records.first.respond_to?(value[:method].to_sym)
                begin
                  valids = []
                  @records.each do |record|
                    valids << record if record.send(value[:method].to_sym)
                  end
                  serializer_method(valids)
                rescue => e
                  render json: { :error => e.message }, status: :unprocessable_entity
                end
              end
            end
          end
        end
      end
    end
  end

  ### BASIC CRUD ACTIONS

  def index
    respond_to do |format|
      format.json do
        serializer_method(@records)
      end
      format.csv do
        export_to_csv(@records)
      end
    end
  end

  def show
    serializer_method(@record)
  end

  def create
    if @record.valid?
      @record.save
      serializer_method(@record)
    else
      previous_record = get_same_previous_record(@record)
      if same_record_discarded?(previous_record)
        previous_record.update_attributes!(record_params)
        previous_record.send(delete_methods[:recover][:method].to_sym)
        serializer_method(previous_record)
      else
        # must raise ActiveRecord::RecordInvalid to catch error
        @record.save!
      end
    end
  end

  def update
    serializer_method(@record.reload) if @record.update_attributes!(record_params)
  end

  def massive_update
    valids = []
    @records.each do |record|
      valids << record if record.update_attributes!(record_params)
    end
    serializer_method(valids)
  end

  private

  def check_registered_actions
    raise ::AbstractController::ActionNotFound.new("#{self.action_name} action is disabled for #{self.class.klass_model.name}") unless registered_actions.include?(self.action_name.to_sym)
  end

  def model_relations
    self.class.klass_model.column_names.select{|n| n.match(/_id$/)}.map{|n| n.gsub('_id','').to_sym}
  end

  def params_attributes(attributes)
    return nil if attributes.nil?
    if attributes.is_a?(Array)
      attributes = attributes.map do |attribute|
        case params[attribute.to_s]
        when ->(param) { param.is_a?(Array) } then { attribute => [] }
        when ->(param) { param.is_a?(Hash) }  then params[attribute].keys.include?('like') ? { attribute.to_sym => :like } : { attribute.to_sym => {} }
        else
          attribute
        end
      end
      params.permit(attributes)
    else
      attribute = attributes
      attribute == :all ? params.permit! : { attribute => params[attribute.to_sym] }
    end
  end

  def enabled_attributes_list(model=nil)
    attributes_list = self.class.enabled_attributes || self.class.klass_model.column_names.map(&:to_sym) - [:created_at, :updated_at]
    attributes_list.map do |attribute|
      params_type = model ? params[model][attribute] : params[attribute]
      params_type.class == ActionController::Parameters ? { "#{attribute}": {} } : attribute
    end
  end

  def record_params
    class_name             = self.class.klass_model.name.demodulize.underscore.to_sym
    versionable_class_name = self.class.klass_model.name.gsub('::','/').underscore.to_sym
    params_model           = params[class_name].present? ? class_name : versionable_class_name
    params.require(params_model).permit(enabled_attributes_list(params_model))
  end

  def get_records
    @records = self.class.klass_model.includes(model_relations).all
    if scopes_filter[:records].any?
      scopes_filter[:records].each do |scope|
        @records = scope[:attributes].nil? ? @records.send(scope[:method].to_sym) : @records.send(scope[:method].to_sym,params_attributes(scope[:attributes]))
      end
    end
    @records = @records.send(delete_methods[:recover][:scope].to_sym) if self.class.soft_delete_method_enabled? and not self.class.return_disabled_records
    if self.class.paginate_state and params[:page]
      response.headers['Access-Control-Expose-Headers'] = 'X-Total, X-Page'
      @records = paginate @records
    end
  end

  def new_record
    @record = self.class.klass_model.new(record_params)
  end

  def get_record
    @record = self.class.klass_model.includes(model_relations)
    if scopes_filter[:record].any?
      scopes_filter[:record].each do |scope|
        @record = scope[:attributes].nil? ? @record.send(scope[:method].to_sym) : @record.send(scope[:method].to_sym,params_attributes(scope[:attributes]))
      end
    end
    raise ActiveRecord::RecordNotFound.new('record not found') unless @record
  end

  def serializer_method(records)
    model = records.respond_to?(:each) ? self.class.klass_model.name.underscore.pluralize : self.class.klass_model.name.underscore
    root  = model.gsub('::','/')

    if available_serializer
      if records.respond_to?(:each)
        if records.any?
          render json: records, each_serializer: available_serializer
        else
          render json: { root.to_sym => records }
        end
      else
        if records.nil?
          render json: { root.to_sym => records }
        else
          render json: records, serializer: available_serializer
        end
      end
    else
      render json: { root.to_sym => records }
    end
  end

  def export_to_csv(records)
    exporter = self.class.available_exporter
    if exporter
      export = exporter.new
      export.collection = records
      response.headers['Filename'] = "#{self.class.klass_model.name.demodulize.underscore.pluralize}.#{Date.today}.#{DateTime.now.hour}-#{DateTime.now.minute}.csv"
      response.headers['Access-Control-Expose-Headers'] = 'Filename'
      send_data export.to_csv, type: 'text/csv'
    else
      response.headers['Content-Type'] = 'application/json'
      raise ActionController::UnknownFormat.new("CSV disabled for #{self.class.klass_model.name.demodulize}")
    end
  end

  def same_record_discarded?(previous_record=@record)
    record = self.class.klass_model.new(previous_record.attributes)
    self.class.soft_delete_method_enabled? and
      !record.valid? and
      record.errors.messages.values.flatten.include?('has already been taken') and
      previous_record.send("#{delete_methods[:soft_delete][:scope]}?".to_sym)
  end

  def get_same_previous_record(record=@record)
    previous_records =  self.class.klass_model.includes(model_relations).
                        where(
                          record.attributes.delete_if do |key, value|
                            next if %i{id uuid}.include?(self.class.klass_model.columns_hash[key].type) and value.present?
                            value.blank? or
                              self.class.klass_model.columns_hash[key].type == :jsonb or
                                !record.errors.messages.key?(key.to_sym)
                          end
                        )
    previous_records[0]
  end

end
