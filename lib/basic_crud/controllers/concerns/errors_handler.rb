module BasicCrud
  module Errors
    extend ::ActiveSupport::Concern

    TYPES = [
              ::StandardError,
              ::ActiveRecord::RecordNotFound,
              ::ActiveRecord::RecordInvalid,
              ::ActionController::ParameterMissing,
              ::ActionController::UnknownFormat
            ]

    included do
      TYPES.each do |error|
        rescue_from error, with: error.to_s.demodulize.underscore.to_sym
      end
    end

    def standard_error(exception)
      render json: { error: exception.message }, status: :internal_server_error
    end

    def record_not_found(exception)
      render json: {error: exception.message}, status: :not_found
    end

    def record_invalid(exception)
      render json: {error: exception.message}, status: :unprocessable_entity
    end

    def parameter_missing(exception)
      render json: { error: exception.message }, status: :precondition_failed
    end

    def unknown_format(exception)
      render json: { error: exception.message }, status: :unsupported_media_type
    end
  end
end